import com.fasterxml.jackson.databind.ObjectMapper;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class App implements ActionListener {
    private static final String THREE_DOTS = "...";
    /**
     * Searches actual state
     */
    private final Searches searches;
    private MenuItem exitItem, addGroupItem;
    /**
     * Configuration json file
     */
    private final File conf;
    private final Desktop desktop;
    private final TrayIcon trayIcon;
    /**
     * Json mapper
     */
    private final ObjectMapper mapper;

    private final ResourceBundle msg = ResourceBundle.getBundle("msg");

    private App(File conf) throws IOException, URISyntaxException {
        this.conf = conf;
        this.mapper = new ObjectMapper();
        final List<Search.Json> rawSearchList = mapper.readValue(conf, mapper.getTypeFactory().constructCollectionType(List.class, Search.Json.class));
        this.searches = Searches.create(rawSearchList);
        this.desktop = Desktop.getDesktop();
        this.trayIcon = new TrayIcon(new ImageIcon(App.class.getResource("find.png"), "systray icon").getImage());

        trayIcon.setPopupMenu(popupMenu());
        try {
            final SystemTray tray = SystemTray.getSystemTray();
            tray.add(trayIcon);
        } catch (AWTException e) {
            System.out.println("TrayIcon could not be added.");
        }
    }

    private PopupMenu popupMenu() throws MalformedURLException, URISyntaxException {
        final PopupMenu popup = new PopupMenu();

        final Menu confMenu = new Menu(msg.getString("settings"));

        addGroupItem = new MenuItem(msg.getString("group.add") + THREE_DOTS);
        addGroupItem.addActionListener(this);
        confMenu.add(addGroupItem);

        boolean someNotEmptyGroups = false;
        final boolean someGroups = !searches.getGroupList().isEmpty();
        if (someGroups) confMenu.addSeparator();

        for (SearchGroup sg : searches.getGroupList()) {
            final boolean empty = sg.getSearchList().isEmpty();

            if (!empty) {
                MenuItem menuItem = new MenuItem(sg.getName());
                sg.setMenuItem(menuItem);
                menuItem.addActionListener(this);
                popup.add(menuItem);
                someNotEmptyGroups = true;
            }

            final Menu groupConfMenu = new Menu(sg.getName());
            confMenu.add(groupConfMenu);

            final MenuItem addToGroupMenuItem = new MenuItem(msg.getString("url.add") + THREE_DOTS);
            addToGroupMenuItem.addActionListener(this);
            sg.setAddItem(addToGroupMenuItem);


            String str = msg.getString("group.remove");
            if (!empty) str += THREE_DOTS;
            final MenuItem removeGroupMenuItem = new MenuItem(str);
            removeGroupMenuItem.addActionListener(this);
            sg.setRemoveItem(removeGroupMenuItem);

            groupConfMenu.add(addToGroupMenuItem);
            groupConfMenu.add(removeGroupMenuItem);
            if (!empty) groupConfMenu.addSeparator();

            for (Search s : sg.getSearchList()) {
                final MenuItem mi = new MenuItem(s.menuName());
                mi.addActionListener(this);
                s.setEditMenuItem(mi);
                groupConfMenu.add(mi);
            }
        }

        if (someNotEmptyGroups) popup.addSeparator();
        popup.add(confMenu);

        popup.addSeparator();
        exitItem = new MenuItem(msg.getString("close"));
        exitItem.addActionListener(this);
        popup.add(exitItem);
        return popup;
    }

    /**
     * Writes actual configuration file and redraws menu.
     * @throws IOException indicates problem with configuration file
     */
    private void refresh() throws IOException, URISyntaxException {
        final List<Search.Json> rawSearches = searches.toJson();
        mapper.writerWithDefaultPrettyPrinter().writeValue(this.conf, rawSearches);
        trayIcon.setPopupMenu(popupMenu());
    }

    public static void main(String[] args) throws IOException, URISyntaxException {
        if (!Desktop.isDesktopSupported() || !SystemTray.isSupported()) System.exit(-1);
        final File conf;
        if (args.length == 0) {
            conf = new File("conf.json");
            if (!conf.isFile()) {
                // create new conf file
                final ObjectMapper mapper = new ObjectMapper();
                mapper.writerWithDefaultPrettyPrinter().writeValue(conf, new Searches());
            }
        } else {
            conf = new File(args[0]);
        }
        new App(conf);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        final Object mi = e.getSource();
        if (mi == exitItem) {
            System.exit(0);
        }

        SearchGroup sg = searches.getGroupList().stream().filter(g -> g.getMenuItem() == mi).findFirst().orElse(null);
        if (sg != null) {
            searchWithGroup(sg);
            return;
        }

        try {
            if (mi == addGroupItem) {
                addGroup();
                refresh();
                return;
            }

            sg = searches.getGroupList().stream().filter(g -> g.getAddItem() == mi).findFirst().orElse(null);
            if (sg != null) {
                addUrlToGroup(sg);
                refresh();
                return;
            }

            sg = searches.getGroupList().stream().filter(g -> g.getRemoveItem() == mi).findFirst().orElse(null);
            if (sg != null) {
                removeGroup(sg);
                refresh();
                return;
            }

            Search s = null;
            for (SearchGroup sg2 : searches.getGroupList()) {
                for (Search search : sg2.getSearchList()) {
                    if (search.getEditMenuItem() == mi) {
                        s = search;
                        sg = sg2;
                        break;
                    }
                }
            }
            if (s != null) {
                editSearch(sg, s);
                refresh();
            }
        } catch (IOException | URISyntaxException e1) {
            e1.printStackTrace();
        }
    }

    private void removeGroup(SearchGroup sg) {
        boolean delete = true;
        if (!sg.getSearchList().isEmpty()) {
            delete = false;
            int answer = JOptionPane.showConfirmDialog(null, msg.getString("group.remove.confirm"), msg.getString("group.remove"), JOptionPane.YES_NO_OPTION);
            if (answer == JOptionPane.YES_OPTION) {
                delete = true;
            }
        }
        if (delete){
            searches.getGroupList().remove(sg);
        }
    }

    private void addGroup() {
        final String newGroup = JOptionPane.showInputDialog(null, msg.getString("group.add.dialog") + ":", msg.getString("group.add"), JOptionPane.QUESTION_MESSAGE);
        if (newGroup != null && !newGroup.isEmpty()) {
            final SearchGroup sg = new SearchGroup(newGroup);
            searches.getGroupList().add(sg);
        }
    }

    private void editSearch(SearchGroup sg, Search s) {
        final String newUrl = (String) JOptionPane.showInputDialog(null, msg.getString("url.edit.dialog") + " " + placeholderSearch() + ":", msg.getString("url.edit"), JOptionPane.QUESTION_MESSAGE, null, null, s.getUrl());
        if (newUrl == null) return;

        if (newUrl.isEmpty()) {
            sg.getSearchList().remove(s);
            return;
        }

        s.setUrl(newUrl);
    }

    private void addUrlToGroup(SearchGroup sg) {
        final String newUrl = JOptionPane.showInputDialog(null, msg.getString("url.add.dialog") + " " + placeholderSearch() + ":", msg.getString("url.add") + ": " + sg.getName(), JOptionPane.QUESTION_MESSAGE);
        if (newUrl == null || newUrl.isEmpty()) return;

        final Search search = new Search(newUrl);
        sg.getSearchList().add(search);
    }

    private void searchWithGroup(SearchGroup sg) {
        final String searchWordText = JOptionPane.showInputDialog(null, msg.getString("search.dialog") + " " + getString("search.help", UserInput.SEP, UserInput.QUOTES) +":", msg.getString("search"), JOptionPane.QUESTION_MESSAGE);
        if (searchWordText == null || searchWordText.isEmpty()) return;

        final UserInput searchWord = new UserInput(searchWordText);
        final List<URL> urls = sg.getUrls(searchWord);

        for (URL url : urls) {
            if (url == null) continue;
            try {
                desktop.browse(url.toURI());
            } catch (IOException | URISyntaxException e1) {
                e1.printStackTrace();
            }
        }
    }

    private String placeholderSearch(){
        return getString("url.add.help", Search.placeholder(0) + ", " + Search.placeholder(1) + ", " + Search.placeholder(2) + THREE_DOTS);
    }

    private String getString(String key, Object... params) {
        try {
            return MessageFormat.format(msg.getString(key), params);
        } catch (MissingResourceException e) {
            return '!' + key + '!';
        }
    }
}
