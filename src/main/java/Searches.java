import lombok.Data;

import java.util.*;

/**
 * Envelope object for json that contains search groups.
 */
@Data
class Searches {
    private final List<SearchGroup> groupList = new ArrayList<>();

    public static Searches create(List<Search.Json> rawSearchList) {
        final Searches searches = new Searches();
        final Map<String, SearchGroup> searchGroupMap = new LinkedHashMap<>();
        for (final Search.Json search : rawSearchList) {
            for (String groupName : search.getGroupNames()) {
                final SearchGroup searchGroup = searchGroupMap.computeIfAbsent(groupName, k -> new SearchGroup(groupName));
                searchGroup.getSearchList().add(search.toSearch());
            }
        }
        searches.getGroupList().addAll(searchGroupMap.values());
        return searches;
    }

    public List<Search.Json> toJson() {
        final Map<Search, Set<String>> map = new LinkedHashMap<>();
        for (final SearchGroup group : getGroupList()) {
            for (Search search : group.getSearchList()) {
                final Set<String> groupNames = map.computeIfAbsent(search, k -> new TreeSet<>());
                groupNames.add(group.getName());
            }
        }
        return map.entrySet().stream().map(e->{
            final Search.Json json = new Search.Json();
            json.setUrl(e.getKey().getUrl());
            json.setGroupNames(e.getValue().toArray(new String[0]));
            return json;
        }).toList();
    }
}
