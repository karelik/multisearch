import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.LinkedList;

/**
 * Represents text inserted to input field by user
 */
@AllArgsConstructor
@Getter
public class UserInput {
    /**
     * Fragments separator
     */
    public static final char SEP = ' ';
    public static final String SEPSTR = String.valueOf(SEP);
    /**
     * Quoting symbol if fragment contains spaces
     */
    public static final char QUOTES = '"';
    /**
     * Raw user input
     */
    private final String text;

    /**
     * @return fragments separated by SEP with stripped QUOTES
     */
    public LinkedList<String> getFragments() {
        final LinkedList<String> fragments = new LinkedList<>();
        fragments.add(text.trim()); // raw text as %s
        if (!text.contains(SEPSTR)) return fragments;

        boolean ignoreSeparators = false;
        StringBuilder str = new StringBuilder();
        for (char ch : text.toCharArray()){
            switch (ch) {
                case SEP -> {
                    if (!ignoreSeparators) {
                        fragments.add(str.toString());
                        str = new StringBuilder();
                    } else {
                        str.append(ch);
                    }
                }
                case QUOTES -> ignoreSeparators = !ignoreSeparators;
                default -> str.append(ch);
            }
        }
        if (!str.isEmpty()){
            fragments.add(str.toString());
        }
        return fragments;
    }
}
