import lombok.Data;

import java.awt.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Represents one searching url
 */
@Data
public class Search {
    public static final String PLACEHOLDER = "%s";
    /**
     * Raw url from configuration json
     */
    private String url;
    /**
     * Menu item for edit of search
     */
    private MenuItem editMenuItem;

    public Search(String url) {
        this.url = url;
    }

    /**
     * @param searchWord
     * @return completed URL
     */
    public URL url(UserInput searchWord) {
        try {
            String url = urlWithAppliedFragments(searchWord.getFragments());
            return new URI(url).toURL();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param fragments user input fragments of text
     * @return url with placeholders replaced by text fragments from user
     */
    private String urlWithAppliedFragments(LinkedList<String> fragments) {
        String url = this.url;
        int index = fragments.size();
        final Iterator<String> it = fragments.descendingIterator(); // descending prevents replacement %s1 in %s10
        while (it.hasNext()){
            final String f = it.next();
            url = url.replaceAll(placeholder(--index), URLEncoder.encode(f, StandardCharsets.UTF_8));
        }
        return url;
    }

    /**
     * Generates placeholders by number. Zero placeholder is %s.
     * @param i
     * @return
     */
    public static String placeholder(int i) {
        String placeholder = PLACEHOLDER;
        if (i > 0) placeholder += Integer.toString(i);
        return placeholder;
    }

    /**
     * Decides between user defined name or automatic from url host.
     * @return
     * @throws MalformedURLException
     */
    public String menuName() throws MalformedURLException, URISyntaxException {
        return new URI(url).toURL().getHost().replaceFirst("www\\.","");
    }

    @Data
    public static class Json {
        private String url;
        private String[] groupNames;

        public Search toSearch() {
            return new Search(url);
        }
    }
}
