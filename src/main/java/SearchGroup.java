import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.awt.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Named group of searches visible in systray menu.
 */
@Data
public class SearchGroup {
    private final String name;
    private final List<Search> searchList = new ArrayList<>();
    /**
     * Menu item to click to search
     */
    @JsonIgnore
    private MenuItem menuItem;
    /**
     * Menu item to click to add new search to group
     */
    @JsonIgnore
    private MenuItem addItem;
    /**
     * Menu item to remove whole group
     */
    @JsonIgnore
    private MenuItem removeItem;

    /**
     * @param searchWord user input
     * @return completed urls for browser
     */
    public List<URL> getUrls(final UserInput searchWord){
        return searchList.stream().map(s->s.url(searchWord)).collect(Collectors.toList());
    }
}
