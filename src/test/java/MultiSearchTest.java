import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class MultiSearchTest {
    @Test
    public void testMergeInputAndUrl(){
        final Search s = new Search("https://www.sbazar.cz/hledej/%s");
        assertEquals("https://www.sbazar.cz/hledej/%C5%BDivot+po+%C5%BEivot%C4%9B",s.url(new UserInput("Život po životě")).toString());
    }

    @Test
    public void testFragments(){
        final UserInput ui = new UserInput("This \"specific test\" is ok ");
        final LinkedList<String> frags = ui.getFragments();
        assertEquals(5, frags.size());
        assertEquals("This \"specific test\" is ok", frags.get(0));
        assertEquals("This", frags.get(1));
        assertEquals("specific test", frags.get(2));
        assertEquals("is", frags.get(3));
        assertEquals("ok", frags.get(4));
    }
}
