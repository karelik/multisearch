# Multisearch
Project can help you with search of multiple webs with one word (e. g. eshops, movies info servers). It is independent of browser (uses default OS browser).

You can use multiple placeholders for search texts %s, %s1, %s2...

It is written in Java so it requires Java Runtime Environment. You can download multisearch_latest.zip and try.

Application appears in system tray and you can control it by right click menu.

Enjoy

### New feature ideas
* Configuration reload as menu item (useful for manual configuration file edit)